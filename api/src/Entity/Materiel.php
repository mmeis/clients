<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MaterielRepository")
 */
class Materiel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer",name="id_materiel")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $nom_materiel;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix_materiel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomMateriel(): ?string
    {
        return $this->nom_materiel;
    }

    public function setNomMateriel(string $nom_materiel): self
    {
        $this->nom_materiel = $nom_materiel;

        return $this;
    }

    public function getPrixMateriel(): ?int
    {
        return $this->prix_materiel;
    }

    public function setPrixMateriel(int $prix_materiel): self
    {
        $this->prix_materiel = $prix_materiel;

        return $this;
    }

    public function toArray(): array
    {
        return [
            "id" => $this->getId(),
            "nom_materiel" => $this->getNomMateriel(),
            "prix_materiel" => $this->getPrixMateriel(),
        ];
    }
}
