<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LienRepository")
 */
class Lien
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id_lien")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Client")
     * @ORM\JoinColumn(nullable=false,referencedColumnName="id_client", name="idlien_client")
     */
    private $idlien_client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Materiel")
     * @ORM\JoinColumn(nullable=false,referencedColumnName="id_materiel", name="idlien_materiel")
     */
    private $idlien_materiel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getIdlienClient(): ?client
    {
        return $this->idlien_client;
    }

    public function setIdlienClient(?client $idlien_client): self
    {
        $this->idlien_client = $idlien_client;

        return $this;
    }

    public function getIdlienMateriel(): ?Materiel
    {
        return $this->idlien_materiel;
    }

    public function setIdlienMateriel(?Materiel $idlien_materiel): self
    {
        $this->idlien_materiel = $idlien_materiel;

        return $this;
    }

    public function toArray()
    {
        return [
            "id" => $this->getId(),
            "idlien_client" => $this->getIdlienClient()->getId(),
            "idlien_materiel" => $this->getIdlienMateriel()->getId(),
            "quantite" => $this->getQuantite(),
        ];
    }
}
