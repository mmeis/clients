<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer",name="id_client")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $libelle_client;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $adresse_client;

    /**
     * @ORM\Column(type="integer")
     */
    private $cp_client;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $ville_client;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelleClient(): ?string
    {
        return $this->libelle_client;
    }

    public function setLibelleClient(string $libelle_client): self
    {
        $this->libelle_client = $libelle_client;

        return $this;
    }

    public function getAdresseClient(): ?string
    {
        return $this->adresse_client;
    }

    public function setAdresseClient(string $adresse_client): self
    {
        $this->adresse_client = $adresse_client;

        return $this;
    }

    public function getCpClient(): ?int
    {
        return $this->cp_client;
    }

    public function setCpClient(int $cp_client): self
    {
        $this->cp_client = $cp_client;

        return $this;
    }

    public function getVilleClient(): ?string
    {
        return $this->ville_client;
    }

    public function setVilleClient(string $ville_client): self
    {
        $this->ville_client = $ville_client;

        return $this;
    }

    public function toArray(): array
    {
        return [
            "id" => $this->getId(),
            "libelle_client" => $this->getLibelleClient(),
            "adresse_client" => $this->getAdresseClient(),
            "cp_client" => $this->getCpClient(),
            "ville_client" => $this->getVilleClient(),
        ];
    }
}
