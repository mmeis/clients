<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Materiel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;


use App\Repository\MaterielRepository;

class MaterielController extends AbstractController
{
    /**
     * @Route("/materiel", name="test", methods={"GET"})
     */
    public function index(MaterielRepository $repo)
    {
        $links = $repo->findAll();

        /* Convert to json camelCase style */
        $res = array();
        foreach ($links as $line) {
            $converted = array();

            foreach ($line->toArray() as $key => $value) {
                $camel_case_key = lcfirst(\str_replace("_", "", \ucwords($key, "_")));

                $converted[$camel_case_key] = $value;
            }
            $res[] = $converted;
        }

        return $this->json([
            'materials' => $res
        ]);
    }

    /**
     * @Route("/materiel", name="create-materiel", methods={"POST"})
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $material = new Materiel();

        $json = json_decode($request->getContent());

        $material->setNomMateriel($json->nomMateriel);
        $material->setPrixMateriel((int) $json->prixMateriel);

        $errors = [];
        if (count($errors) > 0)
        {
            return $this->json(["errors" => $errors], $status=400);
        }
        $entityManager->persist($material);
        $entityManager->flush();
        return $this->json(null, 201);
    }
}
