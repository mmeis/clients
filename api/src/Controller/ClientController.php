<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Repository\ClientRepository;
use App\Entity\Client;

class ClientController extends AbstractController
{
    /**
     * @Route("/clients", name="get_client", methods={"GET"})
     */
    public function index(ClientRepository $repo)
    {
        $links = $repo->findAll();

        /* Convert to json camelCase style */
        $res = array();
        foreach ($links as $line) {
            $converted = array();

            foreach ($line->toArray() as $key => $value) {
                $camel_case_key = lcfirst(\str_replace("_", "", \ucwords($key, "_")));

                $converted[$camel_case_key] = $value;
            }
            $res[] = $converted;
        }

        return $this->json([
            'clients' => $res
        ]);
    }

    /**
     * @Route("/clients", name="create_client", methods={"POST"})
     */
    public function create(Request $request, EntityManagerInterface $entityManager)
    {
        $client = new Client();

        $json = json_decode($request->getContent());

        $client->setLibelleClient($json->libelleClient);
        $client->setAdresseClient($json->adresseClient);
        $client->setCpClient((int)$json->cpClient);
        $client->setVilleClient($json->villeClient);

        $errors = [];//$validator->validate($client);
        if (count($errors) > 0)
        {
            return $this->json(["errors" => $errors], 400);
        }
        $entityManager->persist($client);
        $entityManager->flush();
        return $this->json(null, 201);
    }
}
