<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Lien;
use App\Repository\LienRepository;
use App\Repository\ClientRepository;
use App\Repository\MaterielRepository;

class LienController extends AbstractController
{
    /**
     * @Route("/links/sorted", name="links_sorted")
     */
    public function index(LienRepository $repo)
    {
        $links = $repo->findBySubject();

        /* Convert to json camelCase style */
        $res = array();
        foreach ($links as $line) {
            $converted = array();

            foreach ($line as $key => $value) {
                $camel_case_key = lcfirst(\str_replace("_", "", \ucwords($key, "_")));

                $converted[$camel_case_key] = $value;
            }
            $res[] = $converted;
        }
        return $this->json([
            'links' => $res
        ]);
    }

    /**
     * @Route("/links", name="links_sorted", methods={"GET"})
     */
    public function links(LienRepository $repo)
    {
        $links = $repo->findAll();

        /* Convert to json camelCase style */
        $res = array();
        foreach ($links as $line) {
            $converted = array();

            foreach ($line->toArray() as $key => $value) {
                $camel_case_key = lcfirst(\str_replace("_", "", \ucwords($key, "_")));

                $converted[$camel_case_key] = $value;
            }
            $res[] = $converted;
        }
        return $this->json([
            'links' => $res
        ]);
    }

    /**
     * @Route("/link", name="links", methods={"POST"})
     */
    public function update(Request $request,
            LienRepository $repo, ClientRepository $client_repo, MaterielRepository $material_repo,
            EntityManagerInterface $entityManager)
    {
        $payload = json_decode($request->getContent());
        $response = new JsonResponse();

        $link = $repo->findOneByMaterialAndClient($payload->nomMateriel, $payload->libelleClient);
        if ($link)
        {
            /* There is already a link between client and materiel, update it */
            $link->setQuantite($link->getQuantite() + $payload->quantite);
            $entityManager->flush();
            $response->setStatusCode(200);
        }
        else
        {
            /* There was no previous link between client and material,
                    create this link if required ressources exists */
            $client = $client_repo->findOneBy(["libelle_client" => $payload->libelleClient]);
            $material = $material_repo->findOneBy(["nom_materiel" => $payload->nomMateriel]);

            if ($client && $material)
            {
                $link = new Lien();

                $link->setIdlienClient($client);
                $link->setIdlienMateriel($material);
                $link->setQuantite($payload->quantite);
                $entityManager->persist($link);
                $entityManager->flush();
                $response->setStatusCode(201);
            }
            else
            {
                $reponse->setStatusCode(409);
            }
        }
        return $response; //$response;
    }
}
