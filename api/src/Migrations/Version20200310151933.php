<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200310151933 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE materiel (id_materiel INT AUTO_INCREMENT NOT NULL, nom_materiel VARCHAR(30) NOT NULL, prix_materiel INT NOT NULL, PRIMARY KEY(id_materiel)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lien (id_lien INT AUTO_INCREMENT NOT NULL, idlien_client_id INT NOT NULL, idlien_materiel_id INT NOT NULL, quantite INT NOT NULL, INDEX IDX_A532B4B5EA232052 (idlien_client_id), INDEX IDX_A532B4B51D9A2A66 (idlien_materiel_id), PRIMARY KEY(id_lien)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE client (id_client INT AUTO_INCREMENT NOT NULL, libelle_client VARCHAR(30) NOT NULL, adresse_client VARCHAR(30) NOT NULL, cp_client INT NOT NULL, ville_client VARCHAR(30) NOT NULL, PRIMARY KEY(id_client)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lien ADD CONSTRAINT FK_A532B4B5EA232052 FOREIGN KEY (idlien_client_id) REFERENCES client (id_client)');
        $this->addSql('ALTER TABLE lien ADD CONSTRAINT FK_A532B4B51D9A2A66 FOREIGN KEY (idlien_materiel_id) REFERENCES materiel (id_materiel)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lien DROP FOREIGN KEY FK_A532B4B51D9A2A66');
        $this->addSql('ALTER TABLE lien DROP FOREIGN KEY FK_A532B4B5EA232052');
        $this->addSql('DROP TABLE materiel');
        $this->addSql('DROP TABLE lien');
        $this->addSql('DROP TABLE client');
    }
}
