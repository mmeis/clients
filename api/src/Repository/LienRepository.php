<?php

namespace App\Repository;

use App\Entity\Lien;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Lien|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lien|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lien[]    findAll()
 * @method Lien[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LienRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lien::class);
    }

    public function findBySubject()
    {
        $dql = "
            SELECT
                client.libelle_client,
                SUM(link.quantite) as quantite_total,
                SUM(link.quantite * materiel.prix_materiel) as prix_total
            FROM
                App\Entity\Lien link
            INNER JOIN
                link.idlien_materiel materiel
            INNER JOIN
                link.idlien_client client
            GROUP BY
                client.id
            HAVING
                SUM(link.quantite) > 30
                AND
                SUM(link.quantite * materiel.prix_materiel) > 30000
            ORDER BY
                prix_total DESC";

        return $this->getEntityManager()->createQuery($dql)->getResult();
    }

    public function findOneByMaterialAndClient($material_name, $libelle_client): ?Lien
    {
        return $this->createQueryBuilder('link')
            ->join('link.idlien_materiel', 'materiel')
            ->join('link.idlien_client', 'client')
            ->where('materiel.nom_materiel = :materiel_name')
            ->andWhere('client.libelle_client = :libelle_client')
            ->setParameter('materiel_name', $material_name)
            ->setParameter('libelle_client', $libelle_client)
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Lien[] Returns an array of Lien objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lien
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
