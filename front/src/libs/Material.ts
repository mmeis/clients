export type IdMaterial = number;

export interface IMaterialApi {
  id?: number;
  nomMateriel?: string;
  prixMateriel?: number;
}

export interface IMaterial {
  id?: IdMaterial;
  name: string;
  price: number;
}

export default class Material implements IMaterial {
  id?: number;
  name: string;
  price: number;

  constructor(name: string, price: number) {
    this.name = name;
    this.price = price;
  }

  static fromApi(api: IMaterialApi): Material | Error {
    if (api.nomMateriel === undefined || api.prixMateriel === undefined) {
      return new Error("Cannot create Material instance from api");
    }

    const material = new Material(
      api.nomMateriel,
      api.prixMateriel
    );
    if (api.id !== undefined) {
      material.id = api.id;
    }
    return material;
  }

  toApi(): IMaterialApi {
    const api: IMaterialApi = {
      nomMateriel: this.name,
      prixMateriel: this.price,
    };

    if (this.id) {
      api.id = this.id;
    }
    return api;
  }
}
