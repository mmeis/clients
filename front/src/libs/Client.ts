export type IdClient = number;

/*
 ** Stable fields
 */
export interface IClient {
  id?: IdClient;
  label: string;
  address: string;
  zip: string;
  city: string;
}

/*
 **  Api fields, expect them to be changeable
 */
export interface IClientApi {
  id?: IdClient;
  libelleClient?: string;
  adresseClient?: string;
  cpClient?: string;
  villeClient?: string;
}

export default class Client implements IClient {
  id?: IdClient;
  label: string;
  address: string;
  zip: string;
  city: string;

  constructor(label: string, address: string, zip: string, city: string) {
    this.label = label;
    this.address = address;
    this.zip = zip;
    this.city = city;
  }

  static fromApi(api: IClientApi): Client | Error {
    if (
      api.libelleClient === undefined
      || api.adresseClient === undefined
      || api.cpClient === undefined
      || api.villeClient === undefined
    ) {
      return new Error("A field is missing from IClientApi object");
    }

    const client = new Client(
      api.libelleClient,
      api.adresseClient,
      api.cpClient,
      api.villeClient
    );
    if (api.id !== undefined) {
      client.id = api.id;
    }
    return client;
  }

  toApi(): IClientApi {
    const res: IClientApi = {
      libelleClient: this.label,
      adresseClient: this.address,
      cpClient: this.zip,
      villeClient: this.city
    };

    if (this.id) {
      res.id = this.id;
    }
    return res;
  }
}
