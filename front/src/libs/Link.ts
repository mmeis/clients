import { IdClient } from "./Client";
import { IdMaterial } from "./Material";

export interface ILinkApi {
  id?: number;
  quantite?: number;
  idlienClient?: number;
  idlienMateriel?: number;
}

export interface ILink {
  id?: number;
  quantity: number;
  idClient: IdClient;
  idMaterial: IdMaterial;
}

export default class Link implements ILink {
  id?: number;
  quantity: number;
  idClient: IdClient;
  idMaterial: IdMaterial;

  constructor(quantity: number, idClient: IdClient, idMaterial: IdMaterial) {
    this.quantity = quantity;
    this.idClient = idClient;
    this.idMaterial = idMaterial;
  }

  static fromApi(api: ILinkApi): Link | Error {
    if (
      api.quantite === undefined
      || api.idlienClient === undefined
      || api.idlienMateriel === undefined
    ) {
      return new Error(`Missing one of quantite (${api.quantite}), idlienClient (${api.idlienClient}) or idlienMateriel (${api.idlienMateriel})`);
    }

    const link = new Link(api.quantite, api.idlienClient, api.idlienMateriel);
    if (api.id) {
      link.id = api.id;
    }
    return link;
  }

  toApi(): ILinkApi {
    const api: ILinkApi = {
      quantite: this.quantity,
      idlienClient: this.idClient,
      idlienMateriel: this.idMaterial
    };

    if (this.id) {
      api.id = this.id;
    }
    return api;
  }
}
