import VueRouter from "vue-router";

import Main from "./components/Main.vue"
import Input from "./components/Input.vue"
import ClientDetails from "./components/ClientDetails.vue"

export default [
  { path: "/", component: Main },
  { path: "/home", component: Main },
  { path: "/input", component: Input },
  { path: "/client-details", component: ClientDetails }
];
