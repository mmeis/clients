CREATE TABLE  `client` (
`id_client` INT NOT NULL AUTO_INCREMENT,
`libelle_client` VARCHAR( 30 ) NOT NULL ,
`adresse_client` VARCHAR( 30 ) NOT NULL ,
`cp_client` INT( 5 ) NOT NULL ,
`ville_client` VARCHAR( 30 ) NOT NULL ,
PRIMARY KEY (  `id_client` )
) ENGINE = INNODB;

INSERT INTO `client` (`id_client`, `libelle_client`, `adresse_client`,`cp_client`,`ville_client`) VALUES 
(1, 'TOSHIBA', 'Rue Haute','13000','MARSEILLE'),
(2, 'CANON', 'Rue Basse','75000','PARIS'),
(3, 'XEROX', 'Rue Grande','69000','LYON'),
(4, 'MINOLTA', 'Boulevard Central','31000','TOULOUSE'),
(5, 'KONICA', 'Boulevard du Circulaire','59000','LILLE'),
(6, 'SAGEM', 'Le Grand Hameau','06000','NICE');

CREATE TABLE `materiel` (
  `id_materiel` INT(10) NOT NULL AUTO_INCREMENT,
  `nom_materiel` VARCHAR(30)  NOT NULL,
  `prix_materiel` INT(10) NOT NULL,
  PRIMARY KEY  (`id_materiel`)
) ENGINE=InnoDB ;

INSERT INTO `materiel` (`id_materiel`, `nom_materiel`, `prix_materiel`) VALUES 
(1, 'PHOTOCOPIEUR', 3),
(2, 'IMPRIMANTE', 2),
(3, 'SERVEUR', 2),
(4, 'PORTABLE', 2),
(5, 'RETROPROJECTEUR', 2);

CREATE TABLE `lien` (
  `id_lien` INT(10) NOT NULL AUTO_INCREMENT,
  `idlien_client` INT(10) NOT NULL,
  `idlien_materiel` INT(10) NOT NULL,
  `quantite` float NOT NULL,
  PRIMARY KEY  (`id_lien`),
  UNIQUE KEY `idlien_client` (`idlien_client`,`idlien_materiel`)
) ENGINE=InnoDB  ;
